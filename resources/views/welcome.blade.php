@extends('app')

@section('content')

	<!-- /.carousel -->
	<div class = "row" >
		<div class = "col-xs-2 col-sm-2 col-md-2 col-lg-2" >

		</div >
		<div class = "col-xs-8 col-sm-8 col-md-8 col-lg-8" >
			<div class = "panel panel-default" >
				<div class = "panel-heading" >
					<h3 class = "panel-title" >Search Projects</h3 >
				</div >
				<div class = "panel-body" >
					<div class = "form-group" >
						{!! Form::label('search', 'Search:') !!}
						{!! Form::text('search', null, ['class'=>'form-control']) !!}
						<a href="{{ url('/searchproject') }}"
						   class="btn btn-sm  btn-primary iframe">
							<span class="glyphicon glyphicon-search"></span>
						</a>
					
					</div >
				</div >
			</div >
		</div >
		<div class = "col-xs-2 col-sm-2 col-md-2 col-lg-2" >

		</div >
	</div >

	<!-- Mostra o carrocel -->
	<div class = "row" >
		<div class = "col-xs-4 col-sm-4 col-md-4 col-lg-4" >

		</div >
		<div class = "col-xs-4 col-sm-4 col-md-4 col-lg-4" >
			<div id = "myCarousel" class = "carousel slide" data-ride = "carousel" data-interval = "3000" >
				<div class = "carousel-inner" >
					<div class = "item active" >
						<img src = "images/1.jpg" >

						<div class = "container" >
							<div class = "carousel-caption" >
								<a href = "#" >Substituir pelo nome do projeto.</a >
							</div >
						</div >
					</div >
					<div class = "item" >
						<img src = "images/2.jpg" alt = "" >

						<div class = "container" >
							<div class = "carousel-caption" >
								<a href = "#" >Substituir pelo nome do projeto.</a >
							</div >
						</div >
					</div >
					<div class = "item" >
						<img src = "images/3.jpg" alt = "" >

						<div class = "container" >
							<div class = "carousel-caption" >
								<a href = "#" >Substituir pelo nome do projeto.</a >
							</div >
						</div >
					</div >
				</div >
				<a class = "left carousel-control" href = "#myCarousel" data-slide = "prev" >
					<span class = "glyphicon glyphicon-chevron-left" ></span >
				</a >
				<a class = "right carousel-control" href = "#myCarousel" data-slide = "next" >
					<span class = "glyphicon glyphicon-chevron-right" ></span >
				</a >
			</div >
		</div >
		<div class = "col-xs-4 col-sm-4 col-md-4 col-lg-4" >

		</div >
	</div >

@endsection
