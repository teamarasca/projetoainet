<?php

	/*
	|--------------------------------------------------------------------------
	| Application Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register all of the routes for an application.
	| It's a breeze. Simply tell Laravel the URIs it should respond to
	| and give it the controller to call when that URI is requested.
	|
	*/

	use Illuminate\Support\Facades\Route;

	Route::get('/', 'WelcomeController@index');

	Route::get('home', 'HomeController@index');

	Route::controllers([
		'auth'     => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);

	Route::get('searchproject', 'ProjectController@searchproject');

	Route::get('add-edit-project', 'ProjectController@addeditproject');

	Route::get('searchuser', 'UserController@searchuser');

	Route::get('add-edit-user', 'UserController@addedituser');